
/*
(*) Variables
    (*) Declaraci�n De Variable 
        (*) CONSTANTES 
            (*) Simplemente Constant es que el valor de la variable no se puede cambiar 
                (*) Ejemplo
                           numero Constant  interger:= 23;

            (*) Tipos de Datos
                (*) Tipos Numericos
                
                    (*) -Number : num�rico
                    (*) -Dec, decimal: num�rico decimal
                    (*) -Double precision : doble precisi�n
                    (*) -Integer, int : enteros
                    (*) -Real : reales
                    (*) -Smallint : entero corto
                    (*) -Binary_integer: enteros (importante para los �ndices en las matrices)
                    (*) -Natural: numeros naturales
                    (*) -Positive: n�meros positivos

                        (*) Ejemplos:
                        
                        numero interger:= 23;
                        
                (*) Tipos Alfanum�ricos
                            
                    (*) - VARCHAR2.- Pueden contener cadenas  de caracteres de longitud variable, su sintaxis es:
                    (*) - CHAR.- Cadenas de  caracteres de longitud fija.
                    (*) - LONG.- Es una cadena de longitud variable con una longitud m�xima de 32.760 bytes, son muy similares a las variables VARCHAR2
                    
                        (*) Ejemplos:
                        
                          nombre varchar2(10):= 'Borja';
    
                (*) Tipos Raw 
                
                    (*) - RAW.- Se emplean para almacenar datos binarios
                    (*) - LONG RAW .- Son similares a los datos LONG, la longitud m�xima de una variable LONG RAW es de 32.760 bytes
                    
                        (*) Ejemplos:
                    
                (*) Tipos Rowid
                
                    (*) - ROWID.- En �l se puede almacenar un identificador de columna que es una clave que identifica un�vocamente a cada fila de la base de datos
                    
                        (*) Ejemplos:
                        
                (*) Tipos BOOLEANOS
                
                    (*) - BOOLEANOS.- Solo pueden contener los valores TRUE, FALSE o NULL
                    
                        (*) Ejemplos:
                        
                (*) Tipos Compuestos
                
                    TIPOS COMPUESTOS.- Hay disponibles dos tipos compuestos: registros y tablas. Un tipo compuesto es aque que consta de una serie de componentes. Una variable de tipo compuesto contendr� una  o m�s variables escalares.

                        (*) Ejemplos:                
                
*/

