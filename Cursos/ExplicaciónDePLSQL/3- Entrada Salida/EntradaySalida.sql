SET SERVEROUTPUT ON


/*
(*) Entrada & Salida
        (*) Entrada
            (*) Ejemplo
                        
                Declare

                    TuNombre varchar2(10):='&TuNombre';

                Begin        
                
                END;
                
    (*) Salida
        (*) Ejemplo
            (*) Decir algo
                DBMS_OUTPUT.PUT_LINE('Hola Mundo');
            (*) Poner dentro de una variable 
                DBMS_OUTPUT.PUT_LINE('Hola Mundo:' || nombre|| tunombre);
*/
 Declare
                
TuNombre varchar2(10):='&TuNombre';

nombre varchar2(10):= 'Borja';

Begin
                        
DBMS_OUTPUT.PUT_LINE('Hola Mundo:' || nombre|| tunombre);

END;
                