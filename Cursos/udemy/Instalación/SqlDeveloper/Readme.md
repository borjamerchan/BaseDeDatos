
## BasesDeDatos

   ### Modelo Relaciónal
   - [ ] ModeloRelaciónal
   - [ ] PasoATabla
   ### SQL

   + [ ] Sql:
      - [ ] ConsultasBasicas:
         + [ ] Teoria: [PDF]()
         + [ ] EjerciciosBasicos: [Archivo.sql]()
         + [ ] EjerciciosPracticos: [Archivo.sql]()

      - [ ] FuncionesNumericas:
         + [ ] Teoria: [PDF]()
         + [ ] EjerciciosBasicos: [Archivo.sql]()
         + [ ] EjerciciosPracticos: [Archivo.sql]()
           
      - [ ] FuncionesDeGrupo:
         + [ ] Teoria: [PDF]()
         + [ ] EjerciciosBasicos: [Archivo.sql]()
         + [ ] EjerciciosPracticos: [Archivo.sql]()
         
      - [ ] Flecha:
         + [ ] Teoria: [PDF]()
         + [ ] EjerciciosBasicos: [Archivo.sql]()
         + [ ] EjerciciosPracticos: [Archivo.sql]()
         
      - [ ] ConsultasAnidadas:
         + [ ] Teoria: [PDF]()
         + [ ] EjerciciosBasicos: [Archivo.sql]()
         + [ ] EjerciciosPracticos: [Archivo.sql]()
         
      - [ ] CombinacionesJoin:
         + [ ] Teoria: [PDF]()
         + [ ] EjerciciosBasicos: [Archivo.sql]()
         + [ ] EjerciciosPracticos: [Archivo.sql]()
         
