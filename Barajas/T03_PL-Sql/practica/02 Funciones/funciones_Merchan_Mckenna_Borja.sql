--7�.-  Realizar un bloque Pl/Sql en el que introducimos el numero de un empleado e  incrementamos el salario de dicho empleado en funci�n del n�mero de empleados de los que es jefe. 
   --  Si no es jefe de ning�n empleado  ser� 30 euros.
   --  Si es jefe de un  empleado la subida ser� 50 euros.
   --  Si es jefe de  2 empleados la subida ser� 70 euros.
   --  Si es jefe de m�s de 2 empleados la subida ser� 100 euros.
   --  Adem�s si el empleado es el presidente se incrementar� el salario en 30 euros.
   
declare

	numero_empleado employee.employee_id%type;
	contador_empleados number(7);
	aumento number(4);
	oficio varchar2(15);
    
begin

	numero_empleado := '&numero_empleado';
	aumento := 0;
	
	select function into oficio
	from job
	where job_id = (select job_id
                    from employee
                    where employee_id = numero_empleado);
	
	if oficio = 'PRESIDENT'
		then
			aumento:=30;
	end if;
 
	select count(*) into contador_empleados
	from employee
	where manager_id=numero_empleado;
 
	if contador_empleados = 0
		then 
			aumento := aumento + 30;
		else
        
			if contador_empleados = 1
           			then
                        aumento:=aumento+50;
                        
                    else 
                        if contador_empleados = 2
                  					then 
                                        aumento := aumento + 70;
                                        
                                    else
                                        aumento := aumento + 100;
    					end if;
			end if;	
   	end if;	

   	update employee
   	set salary = salary + aumento
   	where employee_id = numero_empleado;
        dbms_output.put_line(aumento);
    
 end;

select employee_id, last_name, first_name, manager_id, salary
from employee
where employee_id in (7369, 7782, 7507, 7506, 7839);



-- 8�.-  Realizar un bloque Pl/Sql en el que introducimos el n�mero de un empleado y modificamos el salario o la comisi�n  de dicho empleado en funci�n de las siguientes premisas: 
    --  Si el empleado pertenece al departamento 13 su comisi�n ser� la comisi�n media del departamento al que pertenece.
    --  Si el empleado pertenece al departamento 14 su comisi�n ser� la comisi�n m�nima del departamento al que pertenece.
    --  Si el empleado pertenece al departamento 23 su comisi�n ser� la comisi�n m�xima del departamento al que pertenece.
    --  Si el empleado pertenece al departamento 10 su salario ser� el salario m�nimo del departamento al que pertenece.
    --  Si el empleado pertenece al departamento 12 su salario ser� el salario m�ximo del departamento al que pertenece
    --  Si el empleado pertenece a cualquier otro departamento su salario ser� el salario medio del departamento al que pertenece.
    
declare

	numero_empleado employee.employee_id%type;
	departamento employee.department_id%type;
	comision employee.commission%type;
	salario employee.salary%type;
	contador_empleados number(7);
    
begin

	numero_empleado := '&numero_empleado';
	select department_id into departamento
	from employee
	where employee_id = numero_empleado;
  	if departamento = 13
    
		then
            select avg(nvl(commission,0)) into comision
            from employee
            where department_id=departamento;
            
        else
            if departamento=14
                    then
                        select min(nvl(commission,0)) into comision
                        from employee
                        where department_id=departamento;
                        
                    else
                        if departamento=23
                            then
                                select max(nvl(commission,0)) into comision
                                from employee
                                where department_id=departamento;
                        end if;
     			end if;    
  	end if;
    
    if departamento in (13,14,23)
    
        then
            update employee
            set commission = comision
            where employee_id = numero_empleado;
                dbms_output.put_line('El empleado con el n�mero '||numero_empleado|| ' pertenece al departamento '||departamento||' y se le asigna la comision '||comision);
  	end if;

 	if departamento = 10
    
    		then
      			select min(salary) into salario
      			from employee
     			where department_id=departamento;
                
            else
      			if departamento=12
                
                    then
                        select max(salary) into salario
                        from employee
                        where department_id = departamento;
       				 
                     else
                        if departamento not in (10,12,13,14,23)
                        
                            then
                                select avg(salary) into salario
                                from employee
                                where department_id = departamento;
          					end if;
      			end if;
  	end if;
    
	if departamento not in (13,14,23)
    
    		then
      			update employee
      			set salary = salario
      			where employee_id = numero_empleado;
                    dbms_output.put_line('El empleado con el n�mero '||numero_empleado|| ' pertenece al departamento '||departamento||' y se le asigna el salario '||salario);
  	end if;
    
 end;
 
-- 9�.-  Crear una vista denominada salarios_caros, con los mismos campos que la tabla empleados, pero en castellano, en donde vamos a guardar todos los empleados que tienen salarios superiores a 2500.
 
    create view salarios_caros(cod_empleado, apellido, nombre, segundo_nombre, cod_funcion, cod_jefe, fecha_entrada, salario, comision, cod_departamento) as
    select *
    from employee
    where salary > 2500
    with check option;

-- 10�.-  Realizar un bloque Pl/Sql en el que insertemos un empleado a trav�s de la vista cuyo c�digo de empleado sea el c�digo del ultimo empleado m�s uno, de la tabla empleados. Con arreglo a las siguientes premisas:
  -- Si el empleado es del departamento 12 su salario es la media del salario de los empleados del departamento que pertenecen a la vista.
  -- Si el empleado es del departamento 20 su salario es el m�nimo del salario de los empleados del departamento que pertenecen a la vista.
  -- Si el empleado es del departamento 30 su salario es el m�ximo del salario de los empleados del departamento que pertenecen a la vista.
  
    declare

        cod_empleado salarios_caros.cod_empleado%type;
        departamento employee.department_id%type;
        comision employee.commission%type;
        salario employee.salary%type;
        salario1 employee.salary%type;
        apellido salarios_caros.apellido%type;
        nombre salarios_caros.nombre%type;
        segundo_nombre salarios_caros.segundo_nombre%type;
        cod_funcion salarios_caros.cod_funcion%type;
        cod_jefe salarios_caros.cod_jefe%type;
    
    begin

        departamento := '&numero_departamento';
        select max(employee_id +1) into cod_empleado
        from employee;

        if departamento=12
            then
               select avg(salario) into salario1
              from salarios_caros
               where cod_departamento = departamento
               group by cod_departamento;
        
         else
            if departamento=20
            
                then
                    select min(salario) into salario1
                    from salarios_caros
                    where cod_departamento = departamento
                    group by cod_departamento;
                    
                else
                    if departamento=30
                    
                        then
                            select max(salario) into salario1
                            from salarios_caros
                            where cod_departamento = departamento
                            group by cod_departamento;
                    end if;
            end if;    
        end if;    

        apellido := '&apellido';
        nombre := '&nombre';          
        segundo_nombre := '&segundo_nombre';
        cod_funcion := '&cod_funcion';
        cod_jefe := '&cod_jefe';
        comision := '&comision';
    
    
        if departamento = 12
        
            then
                insert into salarios_caros
                values (cod_empleado,apellido,nombre, segundo_nombre,cod_funcion,cod_jefe,sysdate,salario1, comision,departamento);
                    dbms_output.put_line('El empleado con el c�digo '||cod_empleado|| ' pertenece al departamento '||departamento||' y se le ha insertado en la vista con el salario medio de los empleados del departamento al que pertenece, el cual es '||salario1|| ' �');
       		
                else  
                    if departamento = 20
                
                        then
                            insert into salarios_caros
                            values (cod_empleado,apellido,nombre, segundo_nombre,cod_funcion,cod_jefe,sysdate, salario1, comision,departamento);
                                dbms_output.put_line('El empleado con el c�digo '||cod_empleado||' pertenece al departamento '||departamento||' y se le ha insertado en la vista con el salario m�nimo de los empleados del departamento al que pertenece, el cual es '||salario1||' �');
            			
                        else
                            if departamento = 30
                        
                                then
                                    insert into salarios_caros
                                    values (cod_empleado,apellido,nombre, segundo_nombre,cod_funcion, cod_jefe,sysdate, salario1, comision, departamento);
                                        dbms_output.put_line('El empleado con el c�digo '||cod_empleado|| ' pertenece al departamento' ||departamento||' y se le ha insertado en la vista con el salario m�ximo de los empleados del departamento al que pertenece, el cual es '||salario1|| ' �');
                            end if;
                    end if;
        end if;
    
    end;
    

-- 11�.-  Realizar un bloque Pl/Sql en el que introducimos el n�mero de un empleado y actualizamos la �ltima l�nea de pedido, del cliente que ha realizado el pedido de mayor cuant�a. De forma que el campo Quantity pasa a valer 10, 20, 30, 40, 50, 60, 70,60, 90 � 100 seg�n sea la �ltima l�nea 1, 2, 3, 4, 5, 6, 7, 8, 9 � 10.

    declare
    
        numero_empleado employee.employee_id%type;
        orden sales_order.order_id%type;
        cliente customer.customer_id%type;
        n_linea item.item_id%type;
        cantidad number (3);
        
    begin
    
        numero_empleado:='&numero_empleado';
        
        select   sales_order.order_id, customer.customer_id into orden, cliente
        from employee,customer,sales_order
        where employee.employee_id=customer.salesperson_id and 	customer.customer_id=sales_order.customer_id
        and employee_id=numero_empleado and sales_order.total = (select max(total)
                                                                 from sales_order
                                                               	 where sales_order.customer_id  in (select  customer_id
                                                                                                  	from customer
                                                                                                 	where salesperson_id = numero_empleado))            
        select max(item_id) into n_linea
        	from item
            where order_id=orden;
            update item
            set quantity=n_linea*10
            where order_id=orden and item_id=n_linea;
            dbms_output.put_line('El empleado con el n�mero '||numero_empleado||' le ha pedido al cliente '||cliente||' el mayor pedido que tiene como c�digo '||orden||'. Este pedido tiene '||n_linea||' l�neas '||' y la �ltima ha sido actualizada a '||cantidad||' �');
            
    end;