SET SERVEROUTPUT ON


--- 1º.-  Realizar un bloque Pl/Sql que calcule el importe de una factura sabiendo que el IVA a aplicar es del 12% y que si el importe bruto de la factura es superior a 50.000 € se debe realizar un descuento del 5%.

          DECLARE
          importe_bruto NUMBER := 60000;
          iva NUMBER := 0.12;
          descuento NUMBER := 0.05;
          importe_final NUMBER;
        BEGIN

          IF importe_bruto > 50000 THEN
            importe_final := importe_bruto - (importe_bruto * descuento);
        
          ELSE
            importe_final := importe_bruto;
          END IF;
          importe_final := importe_final + (importe_final * iva);
          -- Impresión del resultado

        END;


-- 2º.-  Realizar un bloque Pl/Sql en el que introducimos tres números por teclados y nos los visualiza  de mayor a menor

DECLARE

  num1 NUMBER;
  num2 NUMBER;
  num3 NUMBER;
  mayor NUMBER;
  menor NUMBER;
  medio NUMBER;
BEGIN
  DBMS_OUTPUT.PUT_LINE('Ingrese el primer número:');
  num1 := &num1;
  
  DBMS_OUTPUT.PUT_LINE('Ingrese el segundo número:');
  num2 := &num2;
  
  DBMS_OUTPUT.PUT_LINE('Ingrese el tercer número:');
  num3 := &num3;
  
  IF num1 >= num2 AND num1 >= num3 THEN
    mayor := num1;
    IF num2 >= num3 THEN
      medio := num2;
      menor := num3;
    ELSE
      medio := num3;
      menor := num2;
    END IF;
  ELSIF num2 >= num1 AND num2 >= num3 THEN
    mayor := num2;
    IF num1 >= num3 THEN
      medio := num1;
      menor := num3;
    ELSE
      medio := num3;
      menor := num1;
    END IF;
  ELSE
    mayor := num3;
    IF num1 >= num2 THEN
      medio := num1;
      menor := num2;
    ELSE
      medio := num2;
      menor := num1;
    END IF;
  END IF;
  
  DBMS_OUTPUT.PUT_LINE('Los números en orden descendente son:');
  DBMS_OUTPUT.PUT_LINE(mayor || ' ' || medio || ' ' || menor);
END;

/*
3º.-  Realizar un bloque Pl/Sql que calcule el salario neto semanal de un trabajador en función del número de horas trabajadas y la tasa de impuestos de acuerdo a las siguientes hipótesis:
	Las primeras 35 horas se pagan a tarifa normal
	Las horas que pasen de 35 se pagan 1.5 veces la tarifa normal
	 Las tasas de impuestos son:
	Los primeros 50 dólares son libres de impuestos 
	Los siguientes 40 dólares tienen un 25% de impuestos 
	Los restantes de 45% de impuestos
*/
DECLARE
  horas_trabajadas NUMBER := 45;
  tarifa_normal NUMBER := 10; -- dólares por hora
  salario_bruto NUMBER;
  impuestos NUMBER;
  salario_neto NUMBER;
BEGIN
  IF horas_trabajadas <= 35 THEN
    salario_bruto := horas_trabajadas * tarifa_normal;
  ELSE
    salario_bruto := (35 * tarifa_normal) + ((horas_trabajadas - 35) * (tarifa_normal * 1.5));
  END IF;
  
  IF salario_bruto <= 50 THEN
    impuestos := 0;
  ELSIF salario_bruto <= 90 THEN
    impuestos := (salario_bruto - 50) * 0.25;
  ELSE
    impuestos := (40 * 0.25) + ((salario_bruto - 90) * 0.45);
  END IF;
  
  salario_neto := salario_bruto - impuestos;
  
  DBMS_OUTPUT.PUT_LINE('El salario neto semanal del trabajador es: ' || salario_neto || ' dólares');
END;




/*
4º.-  Realizar un bloque pl/sql que calcule las raíces de la ecuación (ax2 + bx + c = 0) teniendo en cuenta los siguientes casos: 

a. Utilizaremos la fórmula siguiente: x= (-b ± √(b2 – 4ac))/(2a) La expresión d = b2 – 4ac se denomina discriminante. 
a1.   Si d>0 entonces hay dos raíces reales
a2.   Si d=0 entonces hay una raíz real que es  x=-b/(2a)
a3.  Si d<0 entonces hay dos raíces complejas de la forma: x+yi, x-yi. Siendo x el valor –b/2a e y  de √(|b2 -4ac|)/(2a)
*/
DECLARE
  a NUMBER := 1;
  b NUMBER := 5;
  c NUMBER := 6;
  discriminante NUMBER;
  x1 NUMBER;
  x2 NUMBER;
BEGIN
  
  discriminante := (b * b) - (4 * a * c);
  IF discriminante > 0 THEN
    x1 := (-b + SQRT(discriminante)) / (2 * a);
    x2 := (-b - SQRT(discriminante)) / (2 * a);
    DBMS_OUTPUT.PUT_LINE('Las dos raíces reales son: ' || x1 || ' y ' || x2);
  ELSIF discriminante = 0 THEN
    x1 := -b / (2 * a);
    DBMS_OUTPUT.PUT_LINE('La única raíz real es: ' || x1);
  ELSE
    x1 := -b / (2 * a);
    x2 := SQRT(ABS(discriminante)) / (2 * a);
    DBMS_OUTPUT.PUT_LINE('Las dos raíces complejas son: ' || x1 || ' + ' || x2 || 'i y ' || x1 || ' - ' || x2 || 'i');
  END IF;
END;


/*
5º.-  Realizar un bloque pl/sql que nos pida un valor numérico por teclado y nos diga si dicho número es primo o no. Un número es primo si solo es divisible por el mismo y por la unidad.
*/

DECLARE
  num NUMBER;
  divisor NUMBER := 2;
  es_primo BOOLEAN := TRUE;
BEGIN
  DBMS_OUTPUT.PUT('Introduzca un número: ');
  num := &num;
  
  IF num <= 1 THEN
    es_primo := FALSE;
  ELSE
    WHILE divisor <= SQRT(num) LOOP
      IF MOD(num, divisor) = 0 THEN
        es_primo := FALSE;
        EXIT;
      END IF;
      divisor := divisor + 1;
    END LOOP;
  END IF;
  
  IF es_primo THEN
    DBMS_OUTPUT.PUT_LINE(num || ' es un número primo.');
  ELSE
    DBMS_OUTPUT.PUT_LINE(num || ' no es un número primo.');
  END IF;
END;


/*
6º Realizar un bloque pl/sql que nos pida 2 valores numéricos enteros  positivos por teclado y calcule la multiplicación  de dichos números por sumas sucesivas.
*/
DECLARE
  -- Declaración de variables
  num1 NUMBER;
  num2 NUMBER;
  producto NUMBER := 0;
BEGIN
  DBMS_OUTPUT.PUT('Introduzca el primer número: ');
  num1 := &num1;
  
  DBMS_OUTPUT.PUT('Introduzca el segundo número: ');
  num2 := &num2;
  
  FOR i IN 1..num2 LOOP
    producto := producto + num1;
  END LOOP;
  
  DBMS_OUTPUT.PUT_LINE('El producto de ' || num1 || ' y ' || num2 || ' es ' || producto);
END;


 -- 7º.- Realizar un bloque pl/sql que nos pida dos números  enteros positivos por teclado y calcule su división por restas sucesivas.
DECLARE
  dividendo NUMBER;
  divisor NUMBER;
  cociente NUMBER := 0;
BEGIN
  DBMS_OUTPUT.PUT('Introduzca el dividendo: ');
  dividendo := &dividendo;
  
  DBMS_OUTPUT.PUT('Introduzca el divisor: ');
  divisor := &divisor;
  
  WHILE dividendo >= divisor LOOP
    dividendo := dividendo - divisor;
    cociente := cociente + 1;
  END LOOP;
  
  DBMS_OUTPUT.PUT_LINE('La división de ' || &dividendo || ' entre ' || &divisor || ' es ' || cociente || ' con resto ' || dividendo);
END;

 
 
 -- 8º.-  Realizar un bloque pl/sql que nos pida  un numero entero por teclado y calcule su factorial.
 
 DECLARE
  num INTEGER;
  factorial INTEGER := 1;
BEGIN
  DBMS_OUTPUT.PUT('Introduzca un número entero positivo: ');
  num := &num;
  
  FOR i IN 1..num LOOP
    factorial := factorial * i;
  END LOOP;
  
  DBMS_OUTPUT.PUT_LINE('El factorial de ' || &num || ' es ' || factorial);
END;

 
-- 9º.-  Realizar un bloque pl/sql que calcule la suma de los números enteros hasta un número dado introducido por el usuario

DECLARE
  
  num INTEGER;
  suma INTEGER := 0;
BEGIN
  DBMS_OUTPUT.PUT('Introduzca un número entero positivo: ');
  num := &num;
  
  FOR i IN 1..num LOOP
    suma := suma + i;
  END LOOP;
  
  DBMS_OUTPUT.PUT_LINE('La suma de los números enteros hasta ' || &num || ' es ' || suma);
END;


--10º.-  Realizar un bloque pl/sql que calcule las tablas de multiplicar del 1 al 9

DECLARE
  -- Declaración de variables
  num1 INTEGER := 1;
  num2 INTEGER := 1;
BEGIN
  -- Bucle externo para iterar por las tablas de multiplicar del 1 al 9
  WHILE num1 <= 9 LOOP
    -- Bucle interno para iterar por los multiplicadores del 1 al 10
    WHILE num2 <= 10 LOOP
      -- Cálculo y salida del resultado
      DBMS_OUTPUT.PUT_LINE(num1 || ' x ' || num2 || ' = ' || num1 * num2);
      num2 := num2 + 1; -- Incremento del multiplicador
    END LOOP;
    num1 := num1 + 1; -- Incremento del número de la tabla de multiplicar
    num2 := 1; -- Reinicio del multiplicador
  END LOOP;
END;

