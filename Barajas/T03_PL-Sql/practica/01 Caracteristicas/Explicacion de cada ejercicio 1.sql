SET SERVEROUTPUT ON
--- 1º.-  Realizar un bloque Pl/Sql que calcule el importe de una factura sabiendo que el IVA a aplicar es del 12% y que si el importe bruto de la factura es superior a 50.000 € se debe realizar un descuento del 5%.
Aquí te proporciono un bloque PL/SQL que calcula el importe de una factura:

sql
Copy code
DECLARE
  -- Declaración de variables
  importe_bruto NUMBER := 60000;
  iva NUMBER := 0.12;
  descuento NUMBER := 0.05;
  importe_final NUMBER;
BEGIN
  -- Cálculo del importe final
  IF importe_bruto > 50000 THEN
    importe_final := importe_bruto - (importe_bruto * descuento);
  ELSE
    importe_final := importe_bruto;
  END IF;
  
  importe_final := importe_final + (importe_final * iva);
  
  -- Impresión del resultado
  DBMS_OUTPUT.PUT_LINE('El importe final de la factura es: ' || importe_final || ' euros');
END;
En este ejemplo, se declara una variable importe_bruto con un valor de 60.000 euros, un iva del 12% y un descuento del 5%. Luego, se utiliza una estructura condicional IF para comprobar si el importe_bruto es mayor que 50.000 euros. Si es así, se aplica el descuento del 5% utilizando la fórmula importe_bruto - (importe_bruto * descuento). En cualquier otro caso, se deja el importe bruto tal como está.

Después, se agrega el valor del IVA al importe_final utilizando la fórmula importe_final + (importe_final * iva). Finalmente, se imprime el resultado utilizando la función DBMS_OUTPUT.PUT_LINE.




Mi Canal Musica
2º.-  Realizar un bloque Pl/Sql en el que introducimos tres números por teclados y nos los visualiza  de mayor a menor
Aquí te proporciono un bloque PL/SQL que te permite ingresar tres números y mostrarlos en orden descendente (del mayor al menor):

sql
Copy code
DECLARE
  -- Declaración de variables
  num1 NUMBER;
  num2 NUMBER;
  num3 NUMBER;
  mayor NUMBER;
  menor NUMBER;
  medio NUMBER;
BEGIN
  -- Ingreso de los números por teclado
  DBMS_OUTPUT.PUT_LINE('Ingrese el primer número:');
  num1 := &num1;
  
  DBMS_OUTPUT.PUT_LINE('Ingrese el segundo número:');
  num2 := &num2;
  
  DBMS_OUTPUT.PUT_LINE('Ingrese el tercer número:');
  num3 := &num3;
  
  -- Ordenamiento de los números
  IF num1 >= num2 AND num1 >= num3 THEN
    mayor := num1;
    IF num2 >= num3 THEN
      medio := num2;
      menor := num3;
    ELSE
      medio := num3;
      menor := num2;
    END IF;
  ELSIF num2 >= num1 AND num2 >= num3 THEN
    mayor := num2;
    IF num1 >= num3 THEN
      medio := num1;
      menor := num3;
    ELSE
      medio := num3;
      menor := num1;
    END IF;
  ELSE
    mayor := num3;
    IF num1 >= num2 THEN
      medio := num1;
      menor := num2;
    ELSE
      medio := num2;
      menor := num1;
    END IF;
  END IF;
  
  -- Impresión de los números en orden descendente
  DBMS_OUTPUT.PUT_LINE('Los números en orden descendente son:');
  DBMS_OUTPUT.PUT_LINE(mayor || ' ' || medio || ' ' || menor);
END;
En este ejemplo, se declaran cuatro variables: num1, num2, num3, mayor, medio y menor. Luego, se utiliza la función & para ingresar los tres números por teclado.

A continuación, se utiliza una estructura condicional IF para determinar cuál es el número mayor, medio y menor entre los tres ingresados. Para ello, se comparan los tres números y se utilizan estructuras IF-ELSE anidadas.

Finalmente, se imprime el resultado utilizando la función DBMS_OUTPUT.PUT_LINE. El resultado muestra los tres números ingresados en orden descendente, del mayor al menor.




Mi Canal Musica
3º.-  Realizar un bloque Pl/Sql que calcule el salario neto semanal de un trabajador en función del número de horas trabajadas y la tasa de impuestos de acuerdo a las siguientes hipótesis:
	Las primeras 35 horas se pagan a tarifa normal
	Las horas que pasen de 35 se pagan 1.5 veces la tarifa normal
	 Las tasas de impuestos son:
	Los primeros 50 dólares son libres de impuestos 
	Los siguientes 40 dólares tienen un 25% de impuestos 
	Los restantes de 45% de impuestos
Aquí te proporciono un bloque PL/SQL que calcula el salario neto semanal de un trabajador en función del número de horas trabajadas y la tasa de impuestos:

sql
Copy code
DECLARE
  -- Declaración de variables
  horas_trabajadas NUMBER := 45;
  tarifa_normal NUMBER := 10; -- dólares por hora
  salario_bruto NUMBER;
  impuestos NUMBER;
  salario_neto NUMBER;
BEGIN
  -- Cálculo del salario bruto
  IF horas_trabajadas <= 35 THEN
    salario_bruto := horas_trabajadas * tarifa_normal;
  ELSE
    salario_bruto := (35 * tarifa_normal) + ((horas_trabajadas - 35) * (tarifa_normal * 1.5));
  END IF;
  
  -- Cálculo de los impuestos
  IF salario_bruto <= 50 THEN
    impuestos := 0;
  ELSIF salario_bruto <= 90 THEN
    impuestos := (salario_bruto - 50) * 0.25;
  ELSE
    impuestos := (40 * 0.25) + ((salario_bruto - 90) * 0.45);
  END IF;
  
  -- Cálculo del salario neto
  salario_neto := salario_bruto - impuestos;
  
  -- Impresión del resultado
  DBMS_OUTPUT.PUT_LINE('El salario neto semanal del trabajador es: ' || salario_neto || ' dólares');
END;
En este ejemplo, se declaran varias variables: horas_trabajadas, tarifa_normal, salario_bruto, impuestos y salario_neto. La cantidad de horas trabajadas se establece en 45 y la tarifa normal en 10 dólares por hora.

Luego, se utiliza una estructura condicional IF para calcular el salario bruto. Si el número de horas trabajadas es menor o igual a 35, se multiplica por la tarifa normal. Si es mayor, se calcula la tarifa normal para las primeras 35 horas y se multiplica por 1.5 para las horas adicionales.

Después, se utiliza otra estructura condicional IF para calcular los impuestos. Si el salario bruto es menor o igual a 50 dólares, no se aplican impuestos. Si es mayor, se aplican diferentes tasas de impuestos según las cantidades establecidas.

Por último, se calcula el salario neto restando los impuestos del salario bruto y se imprime el resultado utilizando la función DBMS_OUTPUT.PUT_LINE. El resultado muestra el salario neto semanal del trabajador en dólares.




Mi Canal Musica
4º.-  Realizar un bloque pl/sql que calcule las raíces de la ecuación (ax2 + bx + c = 0) teniendo en cuenta los siguientes casos: 

a. Utilizaremos la fórmula siguiente: x= (-b ± √(b2 – 4ac))/(2a) La expresión d = b2 – 4ac se denomina discriminante. 
a1.   Si d>0 entonces hay dos raíces reales
a2.   Si d=0 entonces hay una raíz real que es  x=-b/(2a)
a3.  Si d<0 entonces hay dos raíces complejas de la forma: x+yi, x-yi. Siendo x el valor –b/2a e y  de √(|b2 -4ac|)/(2a)
Aquí te proporciono un bloque PL/SQL que calcula las raíces de una ecuación cuadrática utilizando la fórmula general:

sql
Copy code
DECLARE
  -- Declaración de variables
  a NUMBER := 1;
  b NUMBER := 5;
  c NUMBER := 6;
  discriminante NUMBER;
  x1 NUMBER;
  x2 NUMBER;
BEGIN
  -- Cálculo del discriminante
  discriminante := (b * b) - (4 * a * c);
  
  -- Cálculo de las raíces
  IF discriminante > 0 THEN
    x1 := (-b + SQRT(discriminante)) / (2 * a);
    x2 := (-b - SQRT(discriminante)) / (2 * a);
    DBMS_OUTPUT.PUT_LINE('Las dos raíces reales son: ' || x1 || ' y ' || x2);
  ELSIF discriminante = 0 THEN
    x1 := -b / (2 * a);
    DBMS_OUTPUT.PUT_LINE('La única raíz real es: ' || x1);
  ELSE
    x1 := -b / (2 * a);
    x2 := SQRT(ABS(discriminante)) / (2 * a);
    DBMS_OUTPUT.PUT_LINE('Las dos raíces complejas son: ' || x1 || ' + ' || x2 || 'i y ' || x1 || ' - ' || x2 || 'i');
  END IF;
END;
En este ejemplo, se declaran varias variables: a, b, c, discriminante, x1 y x2. Los valores de a, b y c se establecen en 1, 5 y 6 respectivamente.

Luego, se calcula el discriminante utilizando la fórmula b^2 - 4ac. Después, se utiliza una estructura condicional IF para determinar si el discriminante es mayor que cero, igual a cero o menor que cero.

Si el discriminante es mayor que cero, hay dos raíces reales que se calculan utilizando la fórmula general. Si es igual a cero, hay una única raíz real que se calcula con una variación de la fórmula general. Si es menor que cero, hay dos raíces complejas que se calculan utilizando la fórmula para números complejos.

Por último, se imprime el resultado utilizando la función DBMS_OUTPUT.PUT_LINE. El resultado muestra las raíces de la ecuación cuadrática, que pueden ser reales o complejas, dependiendo del valor del discriminante.




Mi Canal Musica
5º.-  Realizar un bloque pl/sql que nos pida un valor numérico por teclado y nos diga si dicho número es primo o no. Un número es primo si solo es divisible por el mismo y por la unidad.
Aquí te proporciono un bloque PL/SQL que determina si un número ingresado por teclado es primo o no:

sql
Copy code
DECLARE
  -- Declaración de variables
  num NUMBER;
  divisor NUMBER := 2;
  es_primo BOOLEAN := TRUE;
BEGIN
  -- Entrada de datos
  DBMS_OUTPUT.PUT('Introduzca un número: ');
  num := &num;
  
  -- Comprobar si es primo
  IF num <= 1 THEN
    es_primo := FALSE;
  ELSE
    WHILE divisor <= SQRT(num) LOOP
      IF MOD(num, divisor) = 0 THEN
        es_primo := FALSE;
        EXIT;
      END IF;
      divisor := divisor + 1;
    END LOOP;
  END IF;
  
  -- Salida de resultados
  IF es_primo THEN
    DBMS_OUTPUT.PUT_LINE(num || ' es un número primo.');
  ELSE
    DBMS_OUTPUT.PUT_LINE(num || ' no es un número primo.');
  END IF;
END;
En este ejemplo, se declaran tres variables: num, divisor y es_primo. num es el número que se va a comprobar, divisor es el número que se va a utilizar como divisor en el bucle, y es_primo es una bandera que se establece en verdadero si el número es primo.

Primero, se solicita al usuario que introduzca un número utilizando la función &num. Luego, se utiliza una estructura condicional IF para comprobar si el número es menor o igual a 1. Si es así, se establece es_primo en falso y se salta el bucle de comprobación.

Si el número es mayor que 1, se inicia un bucle WHILE que continúa hasta que divisor sea mayor que la raíz cuadrada del número. Dentro del bucle, se utiliza la función MOD para comprobar si el número es divisible por divisor. Si es así, se establece es_primo en falso y se sale del bucle.

Después del bucle, se utiliza otra estructura condicional IF para imprimir el resultado. Si es_primo es verdadero, el número es primo y se muestra un mensaje en la pantalla indicándolo. Si es_primo es falso, el número no es primo y se muestra un mensaje en la pantalla indicándolo.




Mi Canal Musica
6º Realizar un bloque pl/sql que nos pida 2 valores numéricos enteros  positivos por teclado y calcule la multiplicación  de dichos números por sumas sucesivas.
Aquí te proporciono un bloque PL/SQL que solicita al usuario dos valores numéricos enteros positivos por teclado y calcula su multiplicación por sumas sucesivas:

sql
Copy code
DECLARE
  -- Declaración de variables
  num1 NUMBER;
  num2 NUMBER;
  producto NUMBER := 0;
BEGIN
  -- Entrada de datos
  DBMS_OUTPUT.PUT('Introduzca el primer número: ');
  num1 := &num1;
  
  DBMS_OUTPUT.PUT('Introduzca el segundo número: ');
  num2 := &num2;
  
  -- Cálculo del producto
  FOR i IN 1..num2 LOOP
    producto := producto + num1;
  END LOOP;
  
  -- Salida de resultados
  DBMS_OUTPUT.PUT_LINE('El producto de ' || num1 || ' y ' || num2 || ' es ' || producto);
END;
En este ejemplo, se declaran tres variables: num1, num2 y producto. num1 y num2 son los dos valores numéricos introducidos por el usuario, mientras que producto es el resultado de la multiplicación.

Se solicita al usuario que introduzca los dos valores numéricos utilizando las funciones &num1 y &num2.

Luego, se utiliza un bucle FOR para sumar num1 a producto num2 veces. En cada iteración del bucle, se agrega num1 al valor actual de producto.

Finalmente, se utiliza la función DBMS_OUTPUT.PUT_LINE para mostrar el resultado en la pantalla. El mensaje incluye los dos valores numéricos introducidos por el usuario y el producto calculado.




Mi Canal Musica
 -- 7º.- Realizar un bloque pl/sql que nos pida dos números  enteros positivos por teclado y calcule su división por restas sucesivas.
Aquí te proporciono un bloque PL/SQL que solicita al usuario dos valores numéricos enteros positivos por teclado y calcula su división por restas sucesivas:

sql
Copy code
DECLARE
  -- Declaración de variables
  dividendo NUMBER;
  divisor NUMBER;
  cociente NUMBER := 0;
BEGIN
  -- Entrada de datos
  DBMS_OUTPUT.PUT('Introduzca el dividendo: ');
  dividendo := &dividendo;
  
  DBMS_OUTPUT.PUT('Introduzca el divisor: ');
  divisor := &divisor;
  
  -- Cálculo del cociente
  WHILE dividendo >= divisor LOOP
    dividendo := dividendo - divisor;
    cociente := cociente + 1;
  END LOOP;
  
  -- Salida de resultados
  DBMS_OUTPUT.PUT_LINE('La división de ' || &dividendo || ' entre ' || &divisor || ' es ' || cociente || ' con resto ' || dividendo);
END;
En este ejemplo, se declaran tres variables: dividendo, divisor y cociente. dividendo y divisor son los dos valores numéricos introducidos por el usuario, mientras que cociente es el resultado de la división.

Se solicita al usuario que introduzca los dos valores numéricos utilizando las funciones &dividendo y &divisor.

Luego, se utiliza un bucle WHILE para restar divisor a dividendo tantas veces como sea posible, aumentando el valor de cociente en 1 en cada iteración. El bucle se detiene cuando el valor de dividendo es menor que divisor.

Finalmente, se utiliza la función DBMS_OUTPUT.PUT_LINE para mostrar el resultado en la pantalla. El mensaje incluye los dos valores numéricos introducidos por el usuario, el cociente calculado y el resto obtenido después de la división.




Mi Canal Musica
 -- 8º.-  Realizar un bloque pl/sql que nos pida  un numero entero por teclado y calcule su factorial.
Aquí te proporciono un bloque PL/SQL que solicita al usuario un número entero positivo por teclado y calcula su factorial:

sql
Copy code
DECLARE
  -- Declaración de variables
  num INTEGER;
  factorial INTEGER := 1;
BEGIN
  -- Entrada de datos
  DBMS_OUTPUT.PUT('Introduzca un número entero positivo: ');
  num := &num;
  
  -- Cálculo del factorial
  FOR i IN 1..num LOOP
    factorial := factorial * i;
  END LOOP;
  
  -- Salida de resultados
  DBMS_OUTPUT.PUT_LINE('El factorial de ' || &num || ' es ' || factorial);
END;
En este ejemplo, se declaran dos variables: num y factorial. num es el número entero introducido por el usuario, mientras que factorial es el resultado del cálculo.

Se solicita al usuario que introduzca el número entero positivo utilizando la función &num.

Luego, se utiliza un bucle FOR para calcular el factorial de num. El bucle itera desde 1 hasta num, multiplicando el valor de factorial por el valor de la variable i en cada iteración.

Finalmente, se utiliza la función DBMS_OUTPUT.PUT_LINE para mostrar el resultado en la pantalla. El mensaje incluye el número entero introducido por el usuario y el valor del factorial calculado.




Mi Canal Musica
-- 9º.-  Realizar un bloque pl/sql que calcule la suma de los números enteros hasta un número dado introducido por el usuario
Aquí te proporciono un bloque PL/SQL que solicita al usuario un número entero positivo por teclado y calcula la suma de todos los números enteros desde 1 hasta ese número:

sql
Copy code
DECLARE
  -- Declaración de variables
  num INTEGER;
  suma INTEGER := 0;
BEGIN
  -- Entrada de datos
  DBMS_OUTPUT.PUT('Introduzca un número entero positivo: ');
  num := &num;
  
  -- Cálculo de la suma
  FOR i IN 1..num LOOP
    suma := suma + i;
  END LOOP;
  
  -- Salida de resultados
  DBMS_OUTPUT.PUT_LINE('La suma de los números enteros hasta ' || &num || ' es ' || suma);
END;
En este ejemplo, se declaran dos variables: num y suma. num es el número entero introducido por el usuario, mientras que suma es el resultado del cálculo.

Se solicita al usuario que introduzca el número entero positivo utilizando la función &num.

Luego, se utiliza un bucle FOR para calcular la suma de los números enteros desde 1 hasta num. El bucle itera desde 1 hasta num, sumando el valor de i al valor de la variable suma en cada iteración.

Finalmente, se utiliza la función DBMS_OUTPUT.PUT_LINE para mostrar el resultado en la pantalla. El mensaje incluye el número entero introducido por el usuario y el valor de la suma calculada.




Mi Canal Musica
--10º.-  Realizar un bloque pl/sql que calcule las tablas de multiplicar del 1 al 9
Aquí te proporciono un bloque PL/SQL que calcula y muestra las tablas de multiplicar del 1 al 9:

sql
Copy code
DECLARE
  -- Declaración de variables
  num1 INTEGER := 1;
  num2 INTEGER := 1;
BEGIN
  -- Bucle externo para iterar por las tablas de multiplicar del 1 al 9
  WHILE num1 <= 9 LOOP
    -- Bucle interno para iterar por los multiplicadores del 1 al 10
    WHILE num2 <= 10 LOOP
      -- Cálculo y salida del resultado
      DBMS_OUTPUT.PUT_LINE(num1 || ' x ' || num2 || ' = ' || num1 * num2);
      num2 := num2 + 1; -- Incremento del multiplicador
    END LOOP;
    num1 := num1 + 1; -- Incremento del número de la tabla de multiplicar
    num2 := 1; -- Reinicio del multiplicador
  END LOOP;
END;
En este ejemplo, se utilizan dos variables num1 y num2 para iterar por las tablas de multiplicar y los multiplicadores respectivamente.

Se utiliza un bucle externo WHILE para iterar por las tablas de multiplicar del 1 al 9. Dentro de este bucle, se utiliza un bucle interno WHILE para iterar por los multiplicadores del 1 al 10.

En cada iteración del bucle interno se calcula el resultado de la multiplicación y se muestra en la pantalla utilizando la función DBMS_OUTPUT.PUT_LINE.

Una vez que se han calculado todas las multiplicaciones de la tabla de multiplicar actual, se incrementa el valor de num1 y se reinicia el valor de num2 para pasar a la siguiente tabla de multiplicar. Este proceso se repite hasta que se han calculado y mostrado todas las tablas de multiplicar del 1 al 9.



