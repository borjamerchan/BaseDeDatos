/**
@autor: Borja
@Temario: Caracteristicas
*/


/**
    
    1º.-  Realizar un bloque Pl/Sql que calcule el importe de una factura sabiendo que el IVA a aplicar es del 12% y que si el importe bruto de la factura es superior a 50.000 € se debe realizar un descuento del 5%.


    2º.-  Realizar un bloque Pl/Sql en el que introducimos tres números por teclados y nos los visualiza  de mayor a menor


    3º.-  Realizar un bloque Pl/Sql que calcule el salario neto semanal de un trabajador en función del número de horas trabajadas y la tasa de impuestos de acuerdo a las siguientes hipótesis:
        •	Las primeras 35 horas se pagan a tarifa normal
        •	Las horas que pasen de 35 se pagan 1.5 veces la tarifa normal
        •	 Las tasas de impuestos son:
        o	Los primeros 50 dólares son libres de impuestos 
        o	Los siguientes 40 dólares tienen un 25% de impuestos 
        o	Los restantes de 45% de impuestos


    4º.-  Realizar un bloque pl/sql que calcule las raíces de la ecuación (ax2 + bx + c = 0) teniendo en cuenta los siguientes casos: 

        a. Utilizaremos la fórmula siguiente: x= (-b ± √(b2 – 4ac))/(2a) La expresión d = b2 – 4ac se denomina discriminante. 
        a1.   Si d>0 entonces hay dos raíces reales
        a2.   Si d=0 entonces hay una raíz real que es  x=-b/(2a)
        a3.  Si d<0 entonces hay dos raíces complejas de la forma: x+yi, x-yi. Siendo x el valor –b/2a e y  de √(|b2 -4ac|)/(2a)


    5º.-  Realizar un bloque pl/sql que nos pida un valor numérico por teclado y nos diga si dicho número es primo o no. Un número es primo si solo es divisible por el mismo y por la unidad.


    6º.- Realizar un bloque pl/sql que nos pida 2 valores numéricos enteros  positivos por teclado y calcule la multiplicación  de dichos números por sumas sucesivas.



    7º.- Realizar un bloque pl/sql que nos pida dos números  enteros positivos por teclado y calcule su división por restas sucesivas.


    8º.-  Realizar un bloque pl/sql que nos pida  un numero entero por teclado y calcule su factorial.


    9º.-  Realizar un bloque pl/sql que calcule la suma de los números enteros hasta un número dado introducido por el usuario


    10º.-  Realizar un bloque pl/sql que calcule las tablas de multiplicar del 1 al 9

/*
