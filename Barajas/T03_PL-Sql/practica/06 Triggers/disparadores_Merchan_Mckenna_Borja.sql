
-- 1º.- Crear un disparador que impida borrar de la tabla ITEM el dia 11 de cualquier mes.


    
CREATE OR REPLACE TRIGGER Borrar_ITEM
BEFORE DELETE ON ITEM
FOR EACH ROW
DECLARE
  v_day NUMBER;
BEGIN
  v_day := EXTRACT(DAY FROM SYSDATE);

  
  IF v_day = 11 THEN
    RAISE_APPLICATION_ERROR(-20001, 'No se permite borrar registros de la tabla ITEM el día 11 de cualquier mes.');
  END IF;
END;
/


--	2º.-  crear un disparador  que impida que un departamento quede con más de 7 empleados cuando se inserte un nuevo empleado en la tabla EMPLOYEE o se modifique el departamento a un empleado en dicha tabla.

	CREATE OR REPLACE TRIGGER comprobar_employee_count
BEFORE INSERT OR UPDATE OF DEPARTMENT_ID ON employee
FOR EACH ROW
DECLARE
  v_employee_count NUMBER;
BEGIN
  -- Contar el número de empleados en el departamento actual
  SELECT COUNT(*) INTO v_employee_count
  FROM employee
  WHERE DEPARTMENT_ID = :new.DEPARTMENT_ID;

  -- Verificar si el número de empleados excede 7
  IF v_employee_count >= 7 THEN
    -- Mostrar un mensaje de error
    RAISE_APPLICATION_ERROR(-20001, 'No se permite tener más de 7 empleados en un departamento.');
  END IF;
END;
/

--	3º.- Crear un disparador que impida la inserción de productos (description) en la tabla productos si ya existe el producto (description).

CREATE OR REPLACE TRIGGER comprobar_duplicar_producto
BEFORE INSERT ON PRODUCT
FOR EACH ROW
DECLARE
  v_count NUMBER;
BEGIN
  SELECT COUNT(*) INTO v_count
  FROM PRODUCT
  WHERE DESCRIPTION = :new.DESCRIPTION;

  IF v_count > 0 THEN

    RAISE_APPLICATION_ERROR(-20001, 'No se permite la inserción de productos duplicados.');
  END IF;
END;
/


--	4º.-  Crear un disparador que impida que se inserten nombres con todas las letras minúsculas en la tabla clientes.

CREATE OR REPLACE TRIGGER comprobar_minusculas_nombre
BEFORE INSERT ON CUSTOMER
FOR EACH ROW
BEGIN
  IF UPPER(:new.NAME) = :new.NAME THEN
    RAISE_APPLICATION_ERROR(-20001, 'No se permite la inserción de nombres con todas las letras minúsculas.');
  END IF;
END;
/


--	5º.-  Crear un trigger para impedir que, al insertar un empleado, el empleado y su jefe puedan pertenecer a departamentos distintos.

CREATE OR REPLACE TRIGGER comprobar_mismo_departamento
BEFORE INSERT ON empleados
FOR EACH ROW
DECLARE
  v_jefe_DEPARTMENT_ID empleados.DEPARTMENT_ID%TYPE;
BEGIN
  
  SELECT department_id INTO v_jefe_DEPARTMENT_ID
  FROM employee
  WHERE EMPLOYEE_ID = :new.jefe_id;

  IF :new.DEPARTMENT_ID != v_jefe_DEPARTMENT_ID THEN
    RAISE_APPLICATION_ERROR(-20001, 'No se permite que el empleado y su jefe pertenezcan a departamentos distintos.');
  END IF;
END;
/


--	6º.- Crear un disparador que impida  la inserción de pedidos con fecha anterior a la actual.
	CREATE OR REPLACE TRIGGER comprobar_fecha_actual
BEFORE INSERT ON SALES_ORDER
FOR EACH ROW
BEGIN
  -- Verificar si la fecha del pedido es anterior a la fecha actual
  IF :new.ORDER_DATE < SYSDATE THEN
    -- Mostrar un mensaje de error
    RAISE_APPLICATION_ERROR(-20001, 'No se permite la inserción de pedidos con fecha anterior a la actual.');
  END IF;
END;
/



