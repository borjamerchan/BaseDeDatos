
--8


create or replace function suma_salarial(nombre varchar2)
 return number is
 suma employee.salary%type;

begin
 select sum(salary) into suma
 from employee,department
 where employee.department_id=department.department_id
 and name=nombre;
 return suma;

end;/
select name,suma_salarial(name)
from department;



--10

create or replace function get_city_sales return varchar2 is
 cursor c_city is
 select c.city city, count(*) cant
 from sales_order so, customer c
 where so.customer_id = c.customer_id
 group by c.city
 order by cant desc;
 city varchar2(14);
 cant number(6);

 begin
 open c_city;
 fetch c_city into city, cant;
 if c_city%found then

 return city;

 else
 return 'no hay ventas';

 end if;

 close c_city;

end;/
begin
 dbms_output.put_line(get_city_sales);

end;/