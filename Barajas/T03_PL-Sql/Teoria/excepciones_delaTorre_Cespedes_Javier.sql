-- 1
create or replace procedure pdepartamentos

(name_dept department.name%TYPE,
locat_id department.location_id%TYPE) is
num_dept department.department_id%TYPE;
contador integer;
no_ciudad exception;
campos_nulos exception;

begin

    select MAX(department_id)+10 into num_dept
    from department;
    select COUNT(location_id) into contador
    from location
    where location_id = locat_id;
    
    if (name_dept is null or locat_id is null)
        then
            raise campos_nulos;
            
    else
        if contador = 0
            then
                raise no_ciudad;
        
        else
            if contador > 0
                then
                    insert into department values (num_dept, upper(name_dept), locat_id);
                        dbms_output.put_line('Se ha a�adido el departamento '||num_dept||' a la tabla DEPARTMENT.');
                    
            end if;
        end if;
    end if;
    
EXCEPTION

    when no_ciudad then
        dbms_output.put_line('El ID de la ciudad introducida no existe en la base de datos');
        
    when campos_nulos then
        dbms_output.put_line('No se han introducido bien los parametros');

end;/

-- Si los par�metros introducidos son v�lidos --
execute pdepartamentos('Contabilidad', 122);

-- Si el nombre departamento es null --
execute pdepartamentos('', 122);

-- Si la ciudad no existe --
execute pdepartamentos('Contabilidad', 12);


--2 --
create or replace procedure pcustomer

(nombre customer.name%TYPE,
credito customer.credit_limit%TYPE,
empleado customer.salesperson_id%TYPE) is

contador_empleado integer;
num_customer customer.customer_id%TYPE;

begin

    select MAX(customer_id)+1 into num_customer
    from customer;

    select COUNT(employee_id) into contador_empleado
    from employee
    where employee_id = empleado;

    if contador_empleado > 0
        then
            if credito is null
                then
                    raise_application_error (-20010,'El l�mite de cr�dito introducido es nulo.');
            else
                insert into customer (customer_id, name, credit_limit, salesperson_id) values (num_customer, nombre, credito, empleado);
                    dbms_output.put_line ('Se ha creado el cliente '||nombre||'.');
                    
            end if;
            
    else
        raise_application_error (-20011,'El vendedor introducido no existe.');

    end if;

end;/

-- Si los parametros introducidos son validos --
execute pcustomer ('CAMACHOS', 5000, 7369);

-- Si el salesperson no existe -- 
execute pcustomer('CAMACHOS', 5000, 7639);

-- Si el l�mite de credito es null --
execute pcustomer('CAMACHOS', '',7369);

--3.--
    
create or replace procedure pmodtrabajo

(id_job job.job_id%TYPE,
id_emple employee.employee_id%TYPE) is
contador_empleado integer;
contador_trabajo integer;
empleado_no_existe exception;
trabajo_no_existe exception;
empletrab_no_existe exception;

begin

    select COUNT(employee_id) into contador_empleado
    from employee
    where employee_id = id_emple;

    select COUNT(job_id) into contador_trabajo
    from job
    where job_id = id_job;

    if contador_empleado > 0 and contador_trabajo > 0
        then
            update employee
            set job_id = id_job
            where employee_id = id_emple;
                dbms_output.put_line ('Se ha actualizado correctamente el trabajo del empleado '||id_emple||'.');
                
    else
        if contador_empleado = 0 and contador_trabajo = 0
            then
                raise empletrab_no_existe;
                
        elsif contador_empleado = 0
            then
                raise empleado_no_existe;
                
        elsif contador_trabajo = 0
            then
                raise trabajo_no_existe;
                
        end if;
    end if;
    
EXCEPTION

    when empletrab_no_existe then
        dbms_output.put_line('No existen ni el empleado indicado, ni el puesto de trabajo.');
        
    when empleado_no_existe then
        dbms_output.put_line ('No existe el empleado indicado.');
        
    when trabajo_no_existe then
        dbms_output.put_line ('No existe el puesto de trabajo indicado');
        
end;/

-- Si los parametros introducidos son validos --
execute pmodtrabajo(667,7369); 

-- Si no existe el empleado, ni el puesto de trabajo introducidos --
execute pmodtrabajo(666,7777);

-- Si no existe el empleado --
execute pmodtrabajo(670,7777);

-- Si no existe el puesto de trabajo --
execute pmodtrabajo (666,7369);
