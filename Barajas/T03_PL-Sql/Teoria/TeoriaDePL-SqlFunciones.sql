-- Pl/Sql Funciones


/* 
      (*) Explicación:
        (*) 
            (*) output:
    
*/

-- Funciones de Caracteres

-- CHR  chr(x).- Devuelve el carácter equivalente al valor puesto entre paréntesis.

SELECT CHR(67)||CHR(65)||CHR(84) "Gato"
FROM DUAL;

/* 
      (*) Explicación:
        (*) 
            (*) output:
    
*/

    SELECT INITCAP('Borja Merchan') FROM DUAL; 
    
/*
        (*) Explicación:
            (*) Te muestra las dos primeras letras de cada palabra en mayusculas
                (*) output:
                    (*) Borja Merchan
*/






--Ejercicios --


/*

7º.-  Realizar un bloque Pl/Sql en el que introducimos el numero de un empleado e  incrementamos el salario de dicho empleado en función del número de empleados de los que es jefe. 
•	Si no es jefe de ningún empleado  será 30 euros.
•	Si es jefe de un  empleado la subida será 50 euros.
•	Si es jefe de  2 empleados la subida será 70 euros.
•	Si es jefe de más de 2 empleados la subida será 100 euros.
•	Además si el empleado es el presidente se incrementará el salario en 30 euros.

Declare
Nºempleado
Cont empelados
Aumento
Oficio

Begin
Introducir nºempleado
Declarar aumento a cero
Calcular el oficio de ese empleado 
	(guardar en la variable oficio el oficio del 
		Empleado  con select
If (oficion = preeistenete){
	Aumento a +30
}

Cuando empelado tien ese empleado(Select)

If (empleado = 1){
	Empleado +50
}
If (empelado = 2 ){
	Aumento a 70

}

Luego a se hace un update

*/

/*

8º.-  Realizar un bloque Pl/Sql en el que introducimos el número de un empleado y modificamos el salario o la comisión  de dicho empleado en función de las siguientes premisas: 

•	Si el empleado pertenece al departamento 13 su comisión será la comisión media del departamento al que pertenece.
•	Si el empleado pertenece al departamento 14 su comisión será la comisión mínima del departamento al que pertenece.
•	Si el empleado pertenece al departamento 23 su comisión será la comisión máxima del departamento al que pertenece.
•	Si el empleado pertenece al departamento 10 su salario será el salario mínimo del departamento al que pertenece.
•	Si el empleado pertenece al departamento 12 su salario será el salario máximo del departamento al que pertenece
•	Si el empleado pertenece a cualquier otro departamento su salario será el salario medio del departamento al que pertenece.



*/

/*
9º.-  Crear una vista denominada salarios_caros, con los mismos campos que la tabla empleados, pero en castellano, en donde vamos a guardar todos los empleados que tienen salarios superiores a 2500.

*/


/*
10º.-  Realizar un bloque Pl/Sql en el que insertemos un empleado a través de la vista cuyo código de empleado sea el código del ultimo empleado más uno, de la tabla empleados. Con arreglo a las siguientes premisas:
•	Si el empleado es del departamento 12 su salario es la media del salario de los empleados del departamento que pertenecen a la vista.
•	Si el empleado es del departamento 20 su salario es el mínimo del salario de los empleados del departamento que pertenecen a la vista.
•	Si el empleado es del departamento 30 su salario es el máximo del salario de los empleados del departamento que pertenecen a la vista.

*/

/*

11º.-  Realizar un bloque Pl/Sql en el que introducimos el número de un empleado y actualizamos la última línea de pedido, del cliente que ha realizado el pedido de mayor cuantía. De forma que el campo Quantity pasa a valer 10, 20, 30, 40, 50, 60, 70,80, 90 ó 100 según sea la última línea 1, 2, 3, 4, 5, 6, 7, 8, 9 ó 10.

*/
