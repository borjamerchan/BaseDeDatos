
-- 1.1 Seleccionar las columnas c�digo de departamento, nombre, salario, fecha de entrada y trabajo de la tabla de empleados: --
SELECT department_id, last_name, job_id, hire_date, salary
FROM employee;


-- 1.2 . Seleccionar todas las columnas de la tabla de departamentos: --
SELECT *
FROM department;


--2.1. Seleccionar los distintos departamentos que existen en la tabla de empleados.--
SELECT DISTINCT department_id
FROM employee;

-- 2.2 Seleccionar los distintos empleos que hay en cada departamento de la tabla de empleados: --
    SELECT DISTINCT department_id, job_id
    FROM employee;


-- 3.1 Seleccionar aquellas personas cuyo c�digo de funci�n sea el 670.--
    SELECT last_name, job_id, salary, hire_date
    FROM employee
    WHERE job_id= 670;

-- 3.2 Seleccionar aquellas personas que no trabajen en el departamento 30. --

SELECT last_name, job_id, salary, hire_date, department_id
FROM employee
WHERE department_id != 30;

-- 3.3 Seleccionar aquellas personas que ganen más de 2000 dólares. --
SELECT last_name, job_id, salary, hire_date
FROM employee
WHERE salary > 2000;

-- 3.4 Seleccionar aquellas personas que hayan entrado en la empresa antes del 1 de Enero de 1985. --

SELECT last_name, job_id, salary, hire_date
FROM employee
WHERE hire_date < '1-1-85';


-- 3.5 Seleccionar aquellos empleados cuya función es 671 y ganan más de 1500 dólares. --
SELECT last_name, job_id, salary, hire_date
FROM employee
WHERE job_id = 671 AND salary > 1500;

 -- 3.6 Seleccionar aquellos empleados cuya función es 671 o que trabajen en el departamento 30.--
SELECT last_name, job_id, salary, hire_date
FROM employee
WHERE job_id= 671 OR department_id= 30;


-- 3.7 Seleccionar aquellos empleados que se llamen SMITH, ALLEN, SCOTT o MILLER. -- 
SELECT last_name, job_id, salary, hire_date
FROM employee
WHERE last_name IN('SMITH','ALLEN','SCOTT','MILLER');

-- 3.7 Seleccionar aquellos empleados que se llamen SMITH, ALLEN, SCOTT o MILLER.
SELECT last_name, job_id, salary, hire_date
FROM employee
WHERE last_name IN('SMITH','ALLEN','SCOTT','MILLER');

--3.8 Seleccionar aquellos empleados que no se llamen SMITH, ALLEN, SCOTT o MILLER.
SELECT last_name, job_id, salary, hire_date
FROM employee
WHERE last_name NOT IN('SMITH','ALLEN','SCOTT','MILLER');




--3.9 Seleccionar aquellos empleados que trabajen en el departamento 30 y su función
sea 670 o 671
SELECT last_name, job_id, salary, hire_date
FROM employee
WHERE department_id = 30 AND (job_id= 670 OR job_id=671);


-- 3.10 Seleccionar aquellos empleados cuya función sea 670, o que trabajen en el departamento 20 y su función sea 671.
SELECT last_name, job_id, department_id, salary, hire_date
FROM employee
WHERE job_id= 670 OR (department_id=20 AND job_id=671);

--3.11 Seleccionar aquellos empleados que tengan una comisión mayor de 200 y menor de 600.
SELECT last_name, salary, commission
Página 13 de 21
FROM employee
WHERE commission BETWEEN 200 AND 600;

-- 3.12 Seleccionar aquellos empleados que cuyo salario no está comprendido entre 1000 y 2000.
SELECT last_name, salary, commission
FROM employee
WHERE salary NOT BETWEEN 1000 AND 2000;

-- 3.13 Seleccionar aquellos empleados que entraron en la empresa durante el año 1985.
SELECT last_name, hire_date
FROM employee
WHERE hire_date BETWEEN '1-1-85' AND '31-12-85';

-- 3.14 Seleccionar aquellos empleados que tienen comisión:
SELECT last_name, commission, department_id, job_id
FROM employee
WHERE commission IS NOT NULL;


