-- 4, 5, 12, 16, 19, 20, 24, 29, 32  y 38 -- 


-- 1.- Muestra de cada empleado, su nombre y nombre de función que realiza.--

-- 2.- Muestra el código y nombre de los departamentos donde trabaja algún empleado, puede que haya algún departamento que no tiene empleados.--

-- 3.- Visualiza el nombre de los clientes junto al nombre del empleado que es surepresentante de ventas. --

-- 4.- Mostrar de cada empleado su nombre y el grado salarial que le corresponde.--

-- 5.- Nombre de cada empleado y al lado el nombre de la persona que es su jefe.--

-- 6.- Nombre de cada empleado y al lado el nombre de la persona que es su jefe, y si no tiene que aparezca "No tiene jefe".--

-- 7.- Nombre de empleados y nombre del depto donde trabajan de aquellos que cobran comisión.--

-- 8.- Cuantos empleados hay en cada depto de ventas.--

-- 9.- Muestra el nombre de los clientes cuyo representante de ventas es WARD.--

 -- 10.- Muestra el nombre de los empleados que son vendedores (SALESPERSON)--

-- 11.- ¿Cuántos empleados son vendedores? --

-- 12.- ¿Cuál es el valor del pedido de mayor valor que ha realizado el cliente JOCKSPORTS?-- 

-- 13.- Muestra de cada producto, su nombre y precio (LIST_PRICE) que se le aplica actualmente. --

-- 14.- ¿Cuánto dinero se ha recaudado con la venta de ‘ACE TENNIS RACKET I’?.--

-- 15.- Muestra de cada departamento, su nombre y número de empleados que trabajan en él; considera los departamentos con el mismo nombre y en distintas localidades, como un único departamento.--

-- 16.- Visualiza para cada producto, su nombre y total de unidades vendidas de dicho producto.--

-- 17.- Visualiza de cada cliente, su nombre y el valor del pedido de mayor valor que ha realizado hasta el momento.--

-- 18.- Visualiza de cada cliente, su nombre y la suma de los totales de todos los pedidos que ha realizado. --

-- 19.- Mostrar de cada depto su código, su nombre y el nº de empleados que tiene, pero si no tiene empleados que salga un 0. --

-- 20.- ¿Cuántos clientes hay que sólo han realizado un pedido? --

-- 21.- Muestra el nombre del cliente que realizo el pedido de mayor valor.--

-- 22.- Muestra el nombre del producto más caro.--

-- 23.- Muestra el nombre del vendedor con más clientes.--

-- 24.- Muestra el nombre del departamento cuya suma de salarios de empleados es la mayor de todas las sumas.--

-- 25.- Muestra el nombre del cliente que ha realizado el mayor número de pedidos. --

-- 26.- Muestra el nombre del producto más vendido. --

-- 27.- ¿Qué cliente o clientes realizaron el primer pedido a la empresa? --


-- 28.-  Muestra el nombre de cada cliente y el código del pedido (ORDER_ID) que, entre todos los pedidos realizados por el cliente, es el más caro (el de mayor valor en el campo TOTAL).

	
-- 29.-  Muestra de cada cliente y sin repetidos, los códigos de los distintos productos que ha solicitado en sus pedidos.

	
-- 30.-  Mostrar el nombre de cada empleado y nombre de la localidad donde trabaja.

	
-- 31 . -  Seleccionar para cada empleado el nombre, número, número de jefe,  departamento y nombre del jefe.  	 
       

-- 32.-  Calcula cuántos empleados trabajan en el departamento de ventas (SALES) de CHICAGO.

	
-- 33.-  Muestra de cada departamento y de cada localidad, cuántos trabajadores tienen; muestra nombre de localidad, nombre de departamento y número de empleados.

	
-- 34.-  Muestra un listado con el nombre de los vendedores y la comisión que obtendrían si se les pagara un 25% del total facturado a sus clientes.

	
-- 35.-  Muestra un listado con el nombre de los vendedores y la comisión que obtendrían si se les pagara un 25% del total facturado a sus clientes.Para aquellos vendedores que no han vendido, se debe visualizar cero como comisión.

	
-- 36.-  Muestra de cada cliente y sin repetidos, los nombres de los productos que ha solicitado en sus pedidos.
	

-- 37.-  Muestra el nombre del cliente que ha comprado más de 100 unidades del producto ‘ACE TENNIS RACKET II’.

	
--38.-  Muestra de cada departamento, su código, nombre, localidad y nombre del empleado más antiguo en ese departamento.
