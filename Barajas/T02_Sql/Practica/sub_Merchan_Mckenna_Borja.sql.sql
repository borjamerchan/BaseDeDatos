-- 2  Visualiza el nombre del empleado más antiguo de la empresa --
select first_name
from employee
where hire_date = (select min(hire_date)
                        from employee);


-- 4.-  Visualiza los nombres de productos que nunca han sido vendidos. --
select description
from product
where product_id != all (select product_id
from item);


-- 8.-  Visualiza el límite de crédito mínimo que corresponde a los clientes que representa el  vendedor cuyo apellido es TURNER --

select min(credit_limit)
From customer
Where salesperson_id = ( select employee_id
From employee

Where last_name="TURNER"));
--  11.-  Visualiza los nombres de empleados del departamento 20 que cobran más que alguno de los empleados del departamento 13. --
select first_name
from employee
where department_id = 20 and salary > any    (select salary
from employee
where department_id = 13);
 
-- 12.-  Visualiza si algún empleado del 20 cobra más que todos los del 13. --

--20.-  Visualiza el nombre del producto del que se han vendido más unidades. --
 
 select description
from product
where product_id = (select product_id
from item
group by product_id
having sum(quantity) = ( select max(sum(quantity))
from item

group by product_id));

-- 22.-  Visualiza el nombre del departamento con menor sueldo. --

select name, department_id
From department
Where department_id in ( select department_id
From employee
Group by department_id
Having min(salary) = (select min(min(salary))

From employee

-- 24.-  Visualiza el nombre de los clientes (customer)  que son atendidos por vendedores que trabajan en el departamento de ventas de Boston. --

select name
from customer
where salesperson_id in (select employee_id
                        from employee
                        where department_id = (select department_id
                                                from department
                                                where name = 'SALES' and location_id = (select location_id
                                                                                        from location
                                                                                        where regional_group ="BOSTON")));

 
--25.-    Visualiza  la  ciudad  donde  se  ubica  el  departamento  donde  se  paga  más  a  los empleados. --

select regional_group
from location
where location_id = (select location_id
                    from department
                    where department_id = (select department_id
                                            from employee
                                            group by department_id
                                            having sum(salary) = (select max(sum(salary))
                                                                    from employee
                                                                    group by department_id)));


-- 26.-    Visualiza  cuántos  empleados  tiene  el  departamento  que  mas  empleados  tiene  realizando la función vendedor (SALESPERSON) --

                    select count(*)
                    from employee
                    group by department_id
                    having department_id= (select department_id
                    from employee
                    where job_id= (select job_id
                        from job
                        where function='SALESPERSON')
                        group by department_id
                            having count(*)= (select max(count(*))
                            from employee
                            where job_id= (select job_id
                                from job
                                where function=('SALESPERSON')
                                group by department_id));

