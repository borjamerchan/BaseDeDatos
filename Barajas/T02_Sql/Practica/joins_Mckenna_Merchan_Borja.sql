-- 4, 5, 12, 16, 19, 20, 24, 29, 32  y 38 -- 


-- 4.- Mostrar de cada empleado su nombre y el grado salarial que le corresponde.--

select last_name, first_name, Grade_id from employee, salary_grade where (salary>lower_bound) and
(salary<upper_bound)



-- 5.- Nombre de cada empleado y al lado el nombre de la persona que es su jefe.--

SELECT e.first_name as NOMBRE_EMPLEADO, e.last_name as APELLIDO_EMPLEADO,j.first_name as
NOMBRE_JEFE, j.last_name as APELLIDO_JEFE
FROM employee e, employee j
WHERE e.manager_id = j.employee_id;

-- 12.- ¿Cuál es el valor del pedido de mayor valor que ha realizado el cliente JOCKSPORTS?-- 

select max(total) from sales_order, customer where customer.customer_id=sales_order.customer_id and
customer.name like 'JOCKSPORTS' group by sales_order.customer_id


-- 16.- Visualiza para cada producto, su nombre y total de unidades vendidas de dicho producto.--

select product.description, sum(item.total) from product, item where product.product_id=item.product_id
GROUP by product.description order by product.description


-- 19.- Mostrar de cada depto su código, su nombre y el nº de empleados que tiene, pero si no tiene empleados que salga un 0. --


SELECT d.department_id, d.name, nvl(count(e.department_id),0)
FROM department d, employee e
WHERE d.department_id =e.department_id (+)

GROUP BY d.department_id, d.name

-- 20.- ¿Cuántos clientes hay que sólo han realizado un pedido? --
select count( count(*))
from sales_order
group by sales_order.customer_id

having count(*)=1

-- 24.- Muestra el nombre del departamento cuya suma de salarios de empleados es la mayor de todas las sumas.--

select name
from department,employee
where department.department_id=employee.department_id
group by employee.department_id, name
having sum(salary)=(select max(sum(salary))
from employee

group by department_id)

-- 29.-  Muestra de cada cliente y sin repetidos, los códigos de los distintos productos que ha solicitado en sus pedidos.


-- 32.-  Calcula cuántos empleados trabajan en el departamento de ventas (SALES) de CHICAGO.

	
--38.-  Muestra de cada departamento, su código, nombre, localidad y nombre del empleado más antiguo en ese departamento.
