
-- Ejercicios 1 ---

select first_name,salary
from employee
WHERE salary > 100;


-- Ejercicio 2 --

    SELECT   first_name, salary
    FROM employee
    where salary = 3000;
    
    
-- Ejercicio 3  3.-  Mostrar los datos de los empleados que se llamen ALICE. --

    select *
    from employee
    WHERE FIRST_name = 'ALICE'
    
-- Ejercico 4   4.-  Mostrar nombre, apellido, salario y comisi�n de los empleados cuyo apellido empieza por M.--


select first_name, last_name, salary , commission
from employee
where last_name like 'M%'

--   5.-  Mostrar todos los campos de los empleados cuyo salario este entre 1000 y 3000 --

SELECT * 
FROM employee
    where salary BETWEEN  1000 and  3000;

--   6.-  Mostrar el nombre y salario de los empleados cuyo salario no este entre 1000 y 3000 --

select first_name , salary
from employee 
where salary not BETWEEN 1000 AND 3000;

-- 7 Mostrar el nombre, departamento y salario de los empleados que trabajando en el 20 cobren m�s de 1000, m�s los del 30.--

select first_name , department_id, salary
from employee
where (department_id =20 and salary>1000) or (department_id = 30)


-- 8.-  Mostrar el  Nombre y apellido de los empleados que no cobran comisi�n,  Ordenado alfab�ticamente --


select first_name, last_name, commission
from employee
where commission is not null
order by commission desc

--  9.-  Mostrar el nombre de los empleados cuya comisi�n sea superior al 10% de su salario.--

select first_name,salary,commission
from employee
where commission > ((salary/100)*10)


-- 10.-   Visualiza toda la informaci�n de los empleados cuyo apellido comienza con D --
Select *
from employee

where last_name like 'D%';



-- 11.-   Muestra el nombre de los empleados que tienen un salario anual (sin comisi�n) menor a 12.000 --
 -- 12.-  Muestra los distintos nombres de los departamentos ordenados alfab�ticamente -- 
 select distinct name
from department

order by name;

 -- 13.-  Muestra la informaci�n de los empleados de los departamentos 20 y 30 que cobran comisi�n --
         select *
        from employee
        where (department_id = 20 or department_id = 30) and commission > 0;

-- 14.-  Muestra la informaci�n de todos los empleados del departamento 20, y de los  empleados del departamento 30 que cobran comisi�n. --
-- 15.-  �Cu�nto cobrar�a cada empleado si incrementamos su salario (sin comisi�n) un 20%? --
            select first_name,salary, salary + (salary * 20) / 100
            from employee;

-- 16.-  De los empleados de los departamentos 30 y 13, muestra su nombre y salario actual,  lo que cobrar�an si incrementamos �ste un 10% y lo que cobrar�an si decrementamos el salario un 5%. --
 -- 17.-  Muestra el c�digo del departamento donde trabaja el jefe --
 select department_id, manager_id
from employee

where manager_id is null;

-- 18.-  Muestra el nombre de los empleados que tienen como jefe al empleado con c�digo 7788? --
select first_name, manager_id
from employee
where manager_id = 7788;

-- 19.-   Muestra de la tabla de precios todos  los precios que se aplican actualmente--