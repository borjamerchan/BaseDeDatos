-- 9, 15, 19, 21, 23, 29, 30, 32, 34  y 38


--9--
select avg(salary+nvl(commission,0)) "Sueldo"
from employee
where department_id>19 and department_id<30;


--15--
select department_id,salary, count(*)
from employee
group by department_id,salary

having count(*)>1;


--19select avg(salary+nvl(commission,0)) "Sueldo"
from employee
where department_id>19 and department_id<30;
select department_id, count(*)
from employee
where salary > 2000

group by department_id;

--21--
select department_id, max(commission)
from employee
where job_id=669 or job_id=670

group by department_id;



--23--
select department_id,avg(salary)
from employee
group by department_id
having count(*)>2;


--29--

select department_id

from employee

where commission is not null
group by department_id

having count(department_id) > 2;

--30--


select department_id, count(*), min(hire_date)
from employee
where salary>3000
group by department_id;

--32--

select department_id, avg(salary)
from employee
where department_id !=13 and (job_id !=669 and job_id !=670)
group by department_id
having avg(salary)>1900;


--34--

select max(sum(total))
from sales_order
group by customer_id;

--38--

select max(count(*))
from employee
where job_id=670
group by department_id;