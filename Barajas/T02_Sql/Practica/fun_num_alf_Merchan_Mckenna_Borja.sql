
 -- 1. Potencia de 5 elevado a 3.

select power(5,3)
from dual;


-- 2.   Raiz cuadrada de 81.

select sqrt(81)
from dual;


-- 3. Posición ASCII de la letra M.

select ascii ('m')
from dual;


-- 4.  El carácter que le corresponde al ascii 65.

select chr(65)
from dual;


-- 5.  Visualizar los códigos de departamento de todos los empleados (sin que se repitan).

select distinct department_id
from employee;


-- 6. Mostrar nombre y lo que cobra (salario + comisión + 10% del salario).

select first_name, round(salary+nvl(commission,0)+salary*0.1)"Cobra"
from employee;


-- 7.  Si la paga es mensual cuanto cobraría un trabajador con 14 pagas anuales incluida la comisión.

select first_name, round(salary*14 + nvl((commission*14),0))"Salario Anual"
from employee;






-- 8.  Mostrar nombre de los empleados que perderían capital en caso de asignarles un 15% de salario adicional en vez de comisión.

select first_name, salary, salary*1.15, salary+commission
from employee
where round(salary*0.15)<commission;


-- 9. Generar la siguiente salida 

select initcap('HOY ESTA NUBLADO')" El tiempo"
from dual;



-- 10. ¿Qué longitud tiene la cadena “Hoy esta Nublado”

select length('Hoy esta nublado')
from dual;



-- 11. Seleccionar los que se llamen MILLER en mayúsculas.

select *
from employee
where upper(last_name)='MILLER';


-- 12. Visualizar salarios en longitud de 10 con el carácter del $ a la izquierda

select first_name,lpad(salary,10,'$') 
from employee;


-- 13.-  De la tabla precios, visualizar la lista de precios en longitud 15 con el simbolo $ a la derecha

Select list_price, rpad(list_price, 15,’$’)
From price;


-- 14 .  Cambiar las B por J.

select first_name,translate(first_name,'B','J')
from employee;





-- 15 Extraer los caracteres 3º y 4º de los nombres --

select first_name,substr(first_name,3,2) 
from employee;


-- 16.-  Extraer los caracteres 2 a 5 de los nombres de los productos de la tabla de productos

Select description, substr(description,2,4)
From product;


-- 17.-  visualiza los empleados cuyos apellidos empiecen por la letra A

Select first_name, last_name
From employee
Where(substr(last_name,1,1))=’A’;


-- 18.-  Visualiza los empleados cuyo nombre termine por E

Select first name, last_name
From employee
Where substr(first_name, lenght(first_name),1)=’E’;


-- 19. Mostrar de la tabla empleados si aparece middle_initial A que se muestre Alto, y si aparece una M que se muestre mediano y si aparece otra cosa que ponga otros.

select first_name,decode(middle_initial,'A','Alto','M','Medio','Otros')
from employee;


-- 20.  Sacar el puesto de trabajo de cada empleado, utilizando la función decode.

SELECT first_name, decode ( job_id, 667, 'Administrativo', 668,  'Operario',
669, 'Analista', 670, 'Vendedor', 671,' Representante', 672,' Presidente')
FROM employee;


-- 21. Mostrar de la tabla Departamentos, si en el campo LOCATION_ID si aparece 122,  ponga new york, si aparece 123 ponga chicago, si aparece124 ponga dallas y si aparece 167 ponga boston.

Select location_id,decode(location_id,122,‘new york’,123,’chicago’,124,’dallas’, 167,’boston’)
From department;



-- 22.  Visualizar los datos de los empleados cuyas iniciales en nombre y apellido sean consecutivas en el alfabeto.

select first_name,last_name
from employee
where ascii(first_name)=ascii(last_name)-1;


-- 23  Mostrar la última letra del nombre de los empleados.

select first_name,substr(first_name,length(first_name),1)
from employee;


-- 24. Visualiza un listado con todos los empleados donde aparezca de cada uno de ellos: <NOMBRE>  cobra anualmente <cantidad> 

select first_name || ' cobra anualmente ' || (salary * 12)
from employee;


-- 25.-  Visualiza un listado con los productos que estan a la venta en la tabla precios. Debe aparecer:

<cod_prod> es un producto a la venta y su precio es de <list_price>

Select product_id ||’es un producto a la venta y su precio es de’||list_price
From price


-- 26.  Quitar la primera A de los nombres de los empleados independientemente del lugar en el que se encuentren en el nombre.  

select  first_name, substr(first_name, 1, instr(first_name, 'a')-1) || substr(first_name,instr(first_name, 'a')+1)
from employee;


-- 27.-  Quitar la ultima S de los nombres de la tabla clientes independientemente del lugar en el que se encuentre en el nombre

select  name, substr(name,1,(instr(name, 's',-1)-1)) || substr(name,(instr(name, 's',-1)+1))
from customer;
