--3--

select to_char(to_date(add_months(trunc(sysdate, 'year'),12)), 'FM day, DD "de" Month "de" YYYY') as afio_siguiente

from dual;


--4--

select add_months(trunc(sysdate + 365, 'year'), 2)- add_months(trunc(sysdate + 365,
'year'), 1) as fecha

from dual;


--6--
select first_name, round(months_between(sysdate, hire_date))
from employee;

--7--

--12--
select count(*)
from sales_order

where (ship_date- order_date) > 6;

--15--
--17--
    select department_id, avg(salary), count(*)
    from employee

    where months_between(sysdate, hire_date)>250

    group by department_id;

--18--
--19--

    select product_id, max(list_price), min(list_price)
    from price
    where to_char(end_date, 'yyyy')=1989
    group by product_id;

--20--
    select distinct(to_char(order_date,'MM'))
    from sales_order

    where to_char(order_date,'YYYY')=1989
    group by to_char(order_date,'MM')

    having count(*)> 1;

