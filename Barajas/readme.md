 ### Base De Datos
   - [ ] Base De Datos
      + [ ] Diagramas:
         + [ ] Modelo Relación:
           - [ ] Teoria:
               + [ ] [Teoria:]( )
           - [ ] [Ejercicios:]( )

         + [ ] Paso A Tabla:
           - [ ] [Teoria:]
           - [ ] [Ejercicios:]( ) 

      + [ ] SQL:
        - [ ] [ConsultasBasicas:]( )
           + [ ] [Teoria:]( )
           + [ ] [Ejercicios:]( )

        - [ ] [FuncionesNumericas:]( )
           + [ ] [Teoria:]( )
           + [ ] [Ejercicios:]( )
           
        - [ ] [Flechas:]( )
           + [ ] [Teoria:]( )
           + [ ] [Ejercicios:]( )
                      
        - [ ] [ConsultasAnidadas:]( )
           + [ ] [Teoria:]( )
           + [ ] [Ejercicios:]( )
           
        - [ ] [Combinaciones Join:] ( )
           + [ ] [Teoria:]( )
           + [ ] [Ejercicios:]( )
                      
        - [ ] [Munipulación De Tablas:]( )
           + [ ] [Teoria:]( )
           + [ ] [Ejercicios:]( )
           
        - [ ] [Vista:] ( )
           + [ ] [Teoria:]( )
           + [ ] [Ejercicios:]( )
           

      + [ ] PL-SQL:
        - [ ] [Caracteristicas:] ( )
            + [ ] [Teoria:]( )
            + [ ] [Ejercicio:]( )

        - [ ] [Funciones:] ( )
           + [ ] [Teoria:]( )
           + [ ] [Ejercicio:]( )
        - [ ] [Cursores:] ( )
           + [ ] [Teoria:]( )
           + [ ] [Ejercicio:]( )
        - [ ] [SubProgramas:] ( )
           + [ ] [Teoria:]( )
           + [ ] [Ejercicio:]( )
        - [ ] [Excepciones:]( )
           + [ ] [Teoria:]( )
           + [ ] [Ejercicio:]( )